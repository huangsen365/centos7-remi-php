mkdir -p /tmp/default_paths_for_docker/home
mkdir -p /tmp/default_paths_for_docker/home/sshuser/.ssh
chmod 700 /tmp/default_paths_for_docker/home/sshuser/.ssh
chown 1000:1000 /tmp/default_paths_for_docker/home/sshuser/.ssh

mkdir -p /tmp/default_paths_for_docker/root
mkdir -p /tmp/default_paths_for_docker/root/.ssh
chmod 700 /tmp/default_paths_for_docker/root/.ssh

mkdir -p /tmp/default_paths_for_docker/var/www
mkdir -p /tmp/default_paths_for_docker/var/log
mkdir -p /tmp/default_paths_for_docker/etc/httpd/conf2.d
mkdir -p /tmp/default_paths_for_docker/var/www/www.yourdomain.com/www

mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php70/lib/php/opcache
mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php70/lib/php/session
mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php70/lib/php/wsdlcache

mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php71/lib/php/opcache
mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php71/lib/php/session
mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php71/lib/php/wsdlcache

mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php72/lib/php/opcache
mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php72/lib/php/session
mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php72/lib/php/wsdlcache

mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php73/lib/php/opcache
mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php73/lib/php/session
mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php73/lib/php/wsdlcache

mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php74/lib/php/opcache
mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php74/lib/php/session
mkdir -p /tmp/default_paths_for_docker/var/opt/remi/php74/lib/php/wsdlcache

mkdir -p /tmp/default_paths_for_docker/etc/opt/remi/php70/php-fpm2.d
mkdir -p /tmp/default_paths_for_docker/etc/opt/remi/php71/php-fpm2.d
mkdir -p /tmp/default_paths_for_docker/etc/opt/remi/php72/php-fpm2.d
mkdir -p /tmp/default_paths_for_docker/etc/opt/remi/php73/php-fpm2.d
mkdir -p /tmp/default_paths_for_docker/etc/opt/remi/php74/php-fpm2.d

mkdir -p /tmp/default_paths_for_docker/var/spool/cron
